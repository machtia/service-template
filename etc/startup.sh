#!/bin/sh
ARTIFACT_NAME='@ARTIFACT_NAME@'
echo "#########################################################################"
echo "#                      Starting $ARTIFACT_NAME                          #"
echo "#########################################################################"

# Definicion de variables de ambiente necesarias para el servicio.
export CONTEXT_PATH='/'
export JAVA_HOME='@JAVA_HOME@'
export HOME_PATH='@HOME_PATH@'

export VERSION='@VERSION@'
export LOG_LEVEL='INFO'
export LOG_FILE_PATH='@LOG_FILE_PATH@'
export LOG_FILE_NAME="$ARTIFACT_NAME"

export SERVICE_PORT=@SERVICE_PORT@

export JVM_PARAMS="-Xms128M -Xmx256M"

# Inicializacion del servicio
PID="$(ps -efa | grep java | grep $ARTIFACT_NAME | awk '{print $2}')"
if [ ! -z "${PID}" ]; then
	echo "Existe otra instancia corriendo con el PID ${PID}."
	echo "-------------------------------------------------------------------------"
    exit 1
else
    $JAVA_HOME/bin/java ${JVM_PARAMS} -D${ARTIFACT_NAME} -jar ${HOME_PATH}/${ARTIFACT_NAME}-${VERSION}.jar > /dev/null 2>&1 &
    # 20 intentos cada tres segundos para verificar que la aplicación haya inicializado correctamente en máximo 1m
    i=0
    while [ $i -lt 20 ]
    do
        i=`expr $i + 1`
        serviceStatus=$(curl http://localhost:$SERVICE_PORT$CONTEXT_PATH/actuator/health)
        echo "Estado del servicio: $serviceStatus"
        if [ "$serviceStatus" = "{\"status\":\"UP\"}" ]
        then
            echo "Inicializacion completada"
            echo "-------------------------------------------------------------------------"
            exit 0
        fi
        sleep 3s
    done
    echo "No se ha podido inicializar la aplicacion correctamente. A continuación encontrará el log de la aplicación ubicado en $LOG_FILE_PATH/$LOG_FILE_NAME.log"
    cat $LOG_FILE_PATH/$LOG_FILE_NAME.log
    exit 1
fi
