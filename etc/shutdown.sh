#!/bin/sh
ARTIFACT_NAME='@ARTIFACT_NAME@'

echo "#########################################################################"
echo "#                      Stopping $ARTIFACT_NAME                          #"
echo "#########################################################################"

# Definicion de variables utilizadas durante shutdown
LOG_FILE_PATH='@LOG_FILE_PATH@'
OLDER_THAN=30
LOG_DATE=`date "+%Y%m%d-%H%M%S"`

# Da de baja el servicio

PID="$(ps -efa | grep java | grep $ARTIFACT_NAME | awk '{print $2}')"
if [ ! -z "${PID}" ]; then
	echo "Process is alive, sending SIGKILL..."
	kill -9 ${PID}
	echo "SIGKILL was sent"
	echo "-------------------------------------------------------------------------"
else
	echo "Process doesn't exist, verify if process is alive"
	echo "-------------------------------------------------------------------------"
	exit 0
fi

# Comprime logs
echo "Comprimiendo logs..."
cd $LOG_FILE_PATH
tar -zcvf $LOG_FILE_PATH/${ARTIFACT_NAME}-${LOG_DATE}.tar.gz *.log
rm $LOG_FILE_PATH/*.log
echo "-------------------------------------------------------------------------"
echo "Eliminando archivos de mas de $OLDER_THAN dias..."
find $LOG_FILE_PATH -mindepth 1 -mtime +$OLDER_THAN -delete
echo "Listo"
echo "-------------------------------------------------------------------------"
exit 0
