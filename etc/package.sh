#!/bin/sh
ARTIFACT_NAME=$CI_PROJECT_NAME
VERSION=$CI_COMMIT_TAG

echo "Construyendo la version $VERSION"
gradle clean build
echo "Construido"

echo "Empaquetando..."
tar -cvf deployment-package.tar build/libs/$ARTIFACT_NAME-$VERSION.jar
echo "Paquete de despliegue creado"
