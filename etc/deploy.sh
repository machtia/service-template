#!/bin/sh
VERSION=$CI_COMMIT_TAG
ARTIFACT_NAME=$CI_PROJECT_NAME

echo "Sustityendo variables en startup.sh"
sed -i 's#@ARTIFACT_NAME@#'"$ARTIFACT_NAME"'#g' etc/startup.sh
sed -i 's#@JAVA_HOME@#'"$JAVA_HOME"'#g' etc/startup.sh
sed -i 's#@HOME_PATH@#'"$HOME_PATH"'#g' etc/startup.sh
sed -i 's#@VERSION@#'"$VERSION"'#g' etc/startup.sh
sed -i 's#@LOG_FILE_PATH@#'"$LOG_FILE_PATH"'#g' etc/startup.sh
sed -i 's#@SERVICE_PORT@#'"$SERVICE_PORT"'#g' etc/startup.sh

echo "Sustituyendo variables en shutdown.sh"
sed -i 's#@ARTIFACT_NAME@#'"$ARTIFACT_NAME"'#g' etc/shutdown.sh
sed -i 's#@LOG_FILE_PATH@#'"$LOG_FILE_PATH"'#g' etc/shutdown.sh

echo "Agregando HOST $TARGET_HOST a hosts conocidos ssh-keyscan $TARGET_HOST >> ~/.ssh/known_hosts"
ssh-keyscan $TARGET_HOST >> ~/.ssh/known_hosts

echo "Genera archivo con llave de despliegue"
echo "$DEPLOY_PEM" > "$(pwd)/deploy.pem"
chmod 400 "$(pwd)/deploy.pem"

echo "Desplegando bin y shells en $HOME_PATH"
ssh -i "$(pwd)/deploy.pem" "$TARGET_USER@$TARGET_HOST" "mkdir -p $HOME_PATH"
scp -i "$(pwd)/deploy.pem" build/libs/"$ARTIFACT_NAME"-"$VERSION".jar "$TARGET_USER@$TARGET_HOST":"$HOME_PATH"
scp -i "$(pwd)/deploy.pem" etc/startup.sh "$TARGET_USER@$TARGET_HOST":"$HOME_PATH"
scp -i "$(pwd)/deploy.pem" etc/shutdown.sh "$TARGET_USER@$TARGET_HOST":"$HOME_PATH"

echo "Finalizando version anterior..."
ssh -i "$(pwd)/deploy.pem" "$TARGET_USER@$TARGET_HOST" "sh $HOME_PATH"/shutdown.sh

echo "Inicializando version instalada..."
ssh -i "$(pwd)/deploy.pem" "$TARGET_USER@$TARGET_HOST" "sh $HOME_PATH"/startup.sh
