# Prerrequisitos

1. Contar con las siguientes variables de ambiente definidas a nivel de grupo o repositorio:

* **AUTOVERSION_CI_TOKEN** Token de acceso personal con permisos de read_repository y write_repository de alguna cuenta que tenga acceso al grupo o repositorio que se utilizará para autoversionar
* **SONAR_HOST_URL** Dirección URL de Sonarcloud. Por ejemplo: https://sonarcloud.io
* **SONAR_TOKEN** Token, generado en sonarcloud.io, utilizado para escanear los repositorios

# Pasos para crear un repositorio a partir de la plantilla https://gitlab.com/machtia/service-template

1. Crear repositorio para el nuevo repositorio (modulo o proyecto) en Gitlab con un usuario tipo Maintainer o superior
    1. Create blank project
    2. Asignar un project name
    3. Asignar nivel de visibilidad. 
        * Privado para los repositorios que contengan logica de negocio. **Default**
        * Publico aquellos repositorios que **no** contengan logica de negocio y que puedan ser de utilidad para open source
    4. Project configuration
        * Palomear inicializar repositorio con un README.
    5. Crear proyecto.
    6. Configurar que las integraciones a main solo sean cuando el pipeline del MR sea exitoso
        1. Acceder al repositorio en Gitlab mediante un navegador. Por ejemplo: https://gitlab.com/machtia/service-template
        2. Ir a **Settings -> General**
        3. Expander subsección **Merge requests** y palomear **Pipelines must succeed**
    7. Configurar que las integraciones a main puedan ser llevadas a cabo por Maintainers y Developers
        1. Acceder al repositorio en Gitlab mediante un navegador. Por ejemplo: https://gitlab.com/machtia/service-template
        2. Ir a **Settings -> Repository**
        3. Expander la subsección **Protected branches**
        4. Seleccionar **Developers + Maintainers** en **Allowed to merge**
    8. Configurar que los tags sean protegidos
        1. Acceder al repositorio en Gitlab mediante un navegador. Por ejemplo: https://gitlab.com/machtia/service-template
        2. Ir a **Settings -> Repository**
        3. Expander la subsección **Protected tags**
        4. Seleccionar **Tag** y crear el wildcard **`*.*.*`**
        5. Seleccionar **Proteger**

2. Configurar el repositorio con un usuario tipo Developer
    1. Clonar repositorio de proyecto modelo
    ```
    cd /tmp
    git clone git@gitlab.com:machtia/service-template.git
    ```
    2. Copiar contenido de proyecto modelo al nuevo repositorio
    ```
    cd /directorio/donde/tendremos/este/repositorio
    git clone git@gitlab.com:machtia/subgrupo/nombre-del-repo.git
    cd nombre-del-repo
    git checkout -b patch-config-inicial
    cp -r /tmp/service-template/* .
    cp /tmp/service-template/.gitlab-ci.yml .
    cp /tmp/service-template/.gitignore .
    ```
    3. Personalizar `build.gradle` con el editor de preferencia.
        * Asignar el grupo correcto. Por ejemplo `group = 'mx.machtia.ddd'`
        * Asignar una descripcion apropiada. `description = 'Servicio de muestra que implementa DDD'`
    4. Personalizar `README.md` con el editor de preferencia
        * Eliminar instrucciones de creacion de repositorio.
        * Asignar el titulo del repositorio.
        * Incluir alguna descripcion sobre que va el repositorio. **Opcional**
    5. Asignar version inicial en `gradle.properties` con el editor de preferencia: `version=1.0.0`
    6. Asignar nombre del repositorio igual al nombre del proyecto en `settings.gradle` con el editor de preferencia. Por ejemplo `rootProject.name = 'service-template'`
    7. Reubicar y Renombrar la clase main ubicada en src/main/java/mx/machtia/MyApplication.java hacia el paquete asignado en el punto 3. 
    8. Agregar al control de versiones de Git los siguientes archivos.
    ```
    git add build.gradle etc .gitignore .gitlab-ci.yml gradle.properties readme.md settings.gradle src
    ```
    9. Commit & push
    ```
    git commit -m "Configuracion inicial"
    git push --set-upstream origin patch-config-inicial
    ```
    10. Crear MR hacia main
    11. Integrar MR.
    

# Service template

